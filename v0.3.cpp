#include<iostream>
#include<string.h>
#include<fstream>
using namespace std;
fstream in;

char file[20];
char m[10];


void chara(fstream& in)//字符
{
    char str[100];
    int a=0;
	while(in.getline(str,100))
	{
		for (int i = 0; i < strlen(str); i++)
		{
			if ((str[i] >= 'A' && str[i] <= 'z') || (str[i] >= '0' && str[i] <= '9') || str[i] == '.' ||str[i] == ',' || str[i] == '?' || str[i] == '!' || str[i] == '/' || str[i] == '*')	
				a++;
		}
	}
	cout<<a<<endl;
}

void words(fstream& in)//单词 
{
	char str[100];
	int a=0;
	while (in.getline(str, 100))
	{
		for (int i = 0; i < strlen(str); i++)
		{
			if (str[i] >= 'A' && str[i] <= 'z')
				if (str[i + 1] <= 'A' || str[i + 1] >= 'z')			
				a++;
		}
	}
	cout<<a<<endl;
}

void sen(fstream& in)//句子 
{
	char str[100];
	int a=0;
	while (in.getline(str, 100))
	{
		for (int i = 0; i < strlen(str); i++)
		{
			if ((str[i] == '.' && str[i - 1] != '.' && str[i + 1] != '.') || (str[i] == '!' && str[i - 1] != '!')|| (str[i] == '?' && str[i - 1] != '?'))//常规结束符判定 
			a++;
			else if(str[i] == '.' && str[i + 1] == '.' && str[i+2]=='.')//省略号判定 
			a++,i+=3;
		}
	}
	cout<<a<<endl;
}

void line(fstream& in,int e)//代码行、空行、注释行 
{
	char str[100];
	int a=0,b=0,c=0,d,y=0,z=0;//a、b、c分别存储代码行、空行 、注释行的数量，d为空行标志，z为注释行标志，y为注释行单行内结束标志 
	while (in.getline(str, 100))
	{
		y = 0;
		d = 0; 
		for (int i = 0; i < strlen(str); i++)
		{
		    if ((str[i] >= 'A' && str[i] <= 'z') || (str[i] >= '0' && str[i] <= '9') || str[i] == '.' ||str[i] == ',' || str[i] == '?' || str[i] == '!' || str[i] == '/' || str[i] == '*')	
				d++;
			if (str[i] == '/' && str[i + 1] == '*')
				z = 1,y = 1; 
			if (str[i] == '*' && str[i + 1] == '/')
				z = 0;	
		}
		if(z && d!=0 || (z==0 && y) )c++;
		if(d!=0)a++;else b++;
	}
	if (e==0)cout<<a-c<<endl;
	else if(e==1)cout<<b<<endl;
	else if(e==2)cout<<c<<endl;
}

int main()
{
	while(1)
	{
	    cin >> m;           
	    cin >> file;
	    fstream in(file, ios::in);
	    switch(m[1])
	    {
	    	case 'c':chara(in);break;
	    	case 'w':words(in);break;
	    	case 's':sen(in);break;
	    	case 'l':line(in,0);break;
	    	case 'b':line(in,1);break;
	    	case 'o':line(in,2);break;
		}
	    in.close();
    }
	return 0;
}
